With more than 20 years of design and manufacturing experience, BOSTONtec is a leader in ergonomic, modular workstations and custom solutions. We focus on affordable yet customizable designs that increase productivity, ROI and employee safety and satisfaction.

Address: 2700 James Savage Road, Midland, MI 48642, USA

Phone: 989-496-3500

Website: http://www.bostontec.com
